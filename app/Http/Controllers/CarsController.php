<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return new Response(Cars::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $filter = $this->validate($request, [
                'brand' => 'required|string',
                'model' => 'required|string',
                'price' => 'required|numeric',
            ]);

            $car = new Cars($filter);
            $car->save();
        } catch (Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }
        return new Response($car, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Cars $cars
     * @return Response
     */
    public function show(Cars $cars)
    {
        return new Response($cars);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Cars $cars
     * @return Response
     */
    public function update(Request $request, Cars $cars)
    {
        try {
            $filter = $this->validate($request, [
                'brand' => 'required|string',
                'model' => 'required|string',
                'price' => 'required|numeric',
            ]);

            $cars->brand = $filter['brand'];
            $cars->model = $filter['model'];
            $cars->price = $filter['price'];

            $cars->save();
        } catch (Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }
        return new Response($cars, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cars $cars
     * @return Response
     */
    public function destroy(Cars $cars)
    {
        try {
            $cars->delete();
        } catch (Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }

        return new Response(['status' => 'ok'], 400);
    }
}
