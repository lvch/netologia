<?php

namespace Tests\Feature;

use Tests\TestCase;

class TokenTest extends TestCase
{

    public function test_create()
    {
        $data = [
            'email' => 'jreichert@example.net',
            'password'  => 'password',
            'token_name'    => 'test'
        ];

        $response = $this->post('/api/tokens/create', $data);

        $response->assertStatus(200);
    }
}
